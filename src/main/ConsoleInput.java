package main;

import java.util.Scanner;

public class ConsoleInput implements Input {
	private static Scanner scanner = new Scanner(System.in);
	
	public ConsoleInput() {
		super();
	}

	@Override
	public String getText() {
		return scanner.next();
	}
}

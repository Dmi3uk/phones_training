package phone;

import phone.internals.CaseColor;
import phone.internals.Ringtone;

public class Aon extends PolyphonicWiredPhone {

	public Ringtone melody1 = new Ringtone() {

		@Override
		public String getRingtone() {
			return "aon1.mp3";
		}
	};
	public Ringtone melody2 = new Ringtone() {

		@Override
		public String getRingtone() {
			return "aon2.mp3";
		}
	};
	public Ringtone melody3 = new Ringtone() {

		@Override
		public String getRingtone() {
			return "aon3.mp3";
		}
	};

	public Aon(CaseColor caseColor) {
		super(caseColor);
		setRingtone(new Ringtone() {

			private String r = "aon.mp3";

			@Override
			public String getRingtone() {
				return r;
			}
		});
	}
}

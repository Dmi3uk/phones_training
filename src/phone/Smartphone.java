package phone;

import phone.internals.CaseColor;

public abstract class Smartphone extends Celluar {
	private String os;
	private boolean isConnected;

	public Smartphone(CaseColor caseColor) {
		super(caseColor);
	}

	public void connectToNetwork() {
		isConnected=true;
	}
	
	public void disconnectFromNetwork() {
		isConnected=false;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	
}

package phone;

import data.Sms;
import phone.internals.CaseColor;
import phone.internals.Ringtone;

public class Zopo extends Smartphone {	
	private String coverColor;
	private int simSlotCount;
	private int levelChina;
	private String matherial;

	public Zopo(Ringtone ringtone, String coverColor, int simSlotCount, int levelChina, String matherial, CaseColor caseColor) {
		super(caseColor);
		this.ringtone = ringtone;
		this.coverColor = coverColor;
		this.simSlotCount = simSlotCount;
		this.levelChina = levelChina;
		this.matherial = matherial;
	}

	@Override
	public Ringtone createRingtone() {
		return null;
	}
	
}

package phone;

import java.io.Serializable;

import connection.Connector;
import main.Main;
import phone.internals.CaseColor;
import phone.internals.Ringtone;

public abstract class Phone implements Serializable {
	static {
		count = 1;
	}
	private static long count;
	private long id;
	private String number;
	protected Ringtone ringtone;
	private boolean isConnected;
	private CaseColor caseColor;

	public Phone(Ringtone ringtone, CaseColor caseColor) {
		this.id = count++;
		this.ringtone = ringtone;
		this.caseColor = caseColor;
	}

	public void ring() {
		System.out.println("phone #" + id + " is ringing: " + ringtone.getRingtone());
	}

	public Connector makeCall(String calleeNumber) {
		System.out.println("calling " + getNumber() + " to " + calleeNumber);
		return new Connector() {
			
			@Override
			public String send() {
				System.out.println("Type a message from #" + getNumber());
				return Main.input.getText();
			}

			@Override
			public void recieve(String str) {
				System.out.println("#" + getNumber() + " received a message: " + str);
				
			}
		};
	}

	public Connector recieveCall(String callerNumber) {
		System.out.println("recieving call " + getNumber() + " from " + callerNumber);
		ring();
		return new PhoneConnector();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public long getId() {
		return id;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " id=" + id + ", number=" + number + ", ringtone="
				+ ringtone.getRingtone() + " the color is " + caseColor.toString()
				+ (isConnected ? " Connected" : " Disconnected");
	}
	
	private class PhoneConnector implements Connector {

		@Override
		public String send() {
			System.out.println("Type a message from #" + getNumber());
			return Main.input.getText();
		}

		@Override
		public void recieve(String str) {
			System.out.println("#" + getNumber() + " received a message: " + str);
			
		}
			
	}
	
}

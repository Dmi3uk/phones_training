package phone;

import phone.internals.CaseColor;
import phone.internals.Ringtone;

public abstract class PolyphonicWiredPhone extends Wired {

	public PolyphonicWiredPhone(CaseColor caseColor) {
		super(caseColor);
		// TODO Auto-generated constructor stub
	}
	
	public void setRingtone(Ringtone ringtone) {
		this.ringtone = ringtone;
	}

}

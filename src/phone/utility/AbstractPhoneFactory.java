package phone.utility;

import exceptions.PhoneCreationException;
import main.Input;
import phone.Phone;
import phone.internals.CaseColor;

public abstract class AbstractPhoneFactory {
	protected Input input;

	public AbstractPhoneFactory(Input input) {
		this.input = input;
	}
	
	public abstract Phone createPhone(PhoneModel model);
 
}

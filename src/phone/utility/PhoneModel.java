package phone.utility;

import phone.Aon;
import phone.Iphone;
import phone.Yealink;
import phone.Zopo;

public enum PhoneModel {
	AON{
		@Override
		public String toString() {
			return "Aon";
		}
	},
	YEALINK{
		@Override
		public String toString() {
			return "YeaLink";
		}
	},
	IPHONE{
		@Override
		public String toString() {
			return "iPhone";
		}
	},
	ZOPO{
		@Override
		public String toString() {
			return "Zopo";
		}
	};	
}

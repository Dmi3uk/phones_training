package phone;

import data.Sms;
import phone.internals.CaseColor;
import phone.internals.Ringtone;

public class Iphone extends Smartphone {

	public Iphone(CaseColor caseColor) {
		super(caseColor);
		setOs("iOs");
	}

	@Override
	public Ringtone createRingtone() {
		return new DefaultIphoneRingtone();
	}

	private static class DefaultIphoneRingtone implements Ringtone {

		@Override
		public String getRingtone() {
			return "iphone.mp3";
		}
		
	}
}

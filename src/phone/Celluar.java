package phone;

import data.Sms;
import phone.internals.CaseColor;

public abstract class Celluar extends Wireless {
	public Celluar(CaseColor caseColor) {
		super(caseColor);
	}

	public void sendSms(Sms sms) {
		System.out.println("sending sms to " + sms.getSenderNumber());
	}

	public void recieveSms(Sms sms) {
		System.out.println("recieving sms from " + sms.getSenderNumber());
	}

}

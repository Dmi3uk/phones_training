package phone;

import phone.internals.CaseColor;
import phone.internals.Ringtone;

public abstract class Wireless extends Phone {
	private int charge = 30;

	public Wireless(CaseColor caseColor) {
		super(null, caseColor);
		ringtone = createRingtone();
	}
	public abstract Ringtone createRingtone();
}

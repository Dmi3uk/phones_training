package connection;

import java.util.HashMap;
import java.util.Map;

import phone.Phone;

public class Ats {
	private static Ats instance;
	private Map<String, Phone> connectedPhones = new HashMap<>();

	private Ats() {
	}

	public boolean connectPhone(Phone phone) {
		if (connectedPhones.containsKey(phone.getNumber())) {
			return false;
		}

		connectedPhones.put(phone.getNumber(), phone);
		phone.setConnected(true);
		return true;
	}

	public boolean disconnectPhone(Phone phone) {
		phone.setConnected(false);
		return connectedPhones.remove(phone.getNumber()) != null;
	}

	public static Ats getInstance() {
		if (instance == null) {
			instance = new Ats();
		}
		return instance;
	}

	public boolean link(String from, String to) {
		if (!(connectedPhones.containsKey(from) && connectedPhones.containsKey(to))) {
			return false;
		}
		Connector connector1 = connectedPhones.get(from).makeCall(to);
		Connector connector2 = connectedPhones.get(to).recieveCall(from);
		boolean side = true;
		while (true) {
			if (side) {
				String message = connector1.send();
				if (message.equals("0")) {
					break;
				}
				connector2.recieve(message);
			} else {
				String message = connector2.send();
				if (message.equals("0")) {
					break;
				}
				connector1.recieve(message);
			}
			side = !side;
		}
		// connector1
		return true;
	}

}

package data;

public class Sms {
	private long timeRecieved;
	private long timeSent;
	private String text;
	private String senderNumber;

	public Sms() {
	}

	public long getTimeRecieved() {
		return timeRecieved;
	}

	public long getTimeSent() {
		return timeSent;
	}

	public String getText() {
		return text;
	}

	public String getSenderNumber() {
		return senderNumber;
	}

	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}

	public void setTimeRecieved(long timeRecieved) {
		this.timeRecieved = timeRecieved;
	}

	public void setTimeSent(long timeSent) {
		this.timeSent = timeSent;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Sms [timeRecieved=" + timeRecieved + ", timeSent=" + timeSent + ", text=" + text + ", senderNumber="
				+ senderNumber + "]";
	}

}
